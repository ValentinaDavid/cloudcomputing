#pragma once

#include <Response.h>

class MostCancellationsResponse : public Framework::Response
{
public:
	MostCancellationsResponse();

	std::string interpretPacket(const boost::property_tree::ptree& packet);
};
