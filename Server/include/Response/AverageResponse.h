#pragma once

#include <Response.h>

class AverageResponse : public Framework::Response
{
public:
	AverageResponse();

	std::string interpretPacket(const boost::property_tree::ptree& packet);
};
