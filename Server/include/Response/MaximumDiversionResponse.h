#pragma once

#include<Response.h>

class MaximumDiversionResponse : public Framework::Response
{
public:
	MaximumDiversionResponse();

	std::string interpretPacket(const boost::property_tree::ptree& packet);
};