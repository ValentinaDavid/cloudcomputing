#pragma once

#include <Response.h>

class TotalAirResponse : public Framework::Response
{
public:
	TotalAirResponse();

	std::string interpretPacket(const boost::property_tree::ptree& packet);
};