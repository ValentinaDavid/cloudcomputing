#pragma once

#include <Response.h>

class MostVisitedResponse : public Framework::Response
{
public:
	MostVisitedResponse();

	std::string interpretPacket(const boost::property_tree::ptree& packet);
};