#pragma once

#include <Response.h>

class ShortestFlightsCancelledResponse : public Framework::Response
{
public:
	ShortestFlightsCancelledResponse();

	std::string interpretPacket(const boost::property_tree::ptree& packet);
};
