#pragma once

#include <Util/DelayedFlight.h>

#include <boost/property_tree/ptree.hpp>

#include <vector>

typedef boost::property_tree::ptree prop_tree;

class CsvParser
{
public:
  CsvParser(const std::string& filename);

  std::vector<std::string> splitFields(const std::string& csvFile);

  std::string getXmlFromCsv(const std::string& csvFile, const std::vector<std::string>& fieldNames, const std::string& itemTag = "Line",
    const std::string& defFieldTag = "Field");

  std::string getXmlFromCsv(std::istream& csvStream, const std::string& itemTag = "Line",
    bool hasHeader = true, const std::string& defFieldTag = "Field");

  void parseXml();

  void setXmlFile(const std::string& xmlFile);

  std::vector<DelayedFlight> getFlights();

private:
  std::string m_csvFile;
  std::string m_xmlFile;
  prop_tree m_tree{};
  std::vector<DelayedFlight> m_flights{};
};