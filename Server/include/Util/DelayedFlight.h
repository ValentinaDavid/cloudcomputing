#pragma once

#include <iostream>

class DelayedFlight
{
public:
	DelayedFlight() = default;

	void setYear(const int& year);
	void setMonth(const int& month);
	void setDayOfMonth(const int& dayOfMonth);
	void setDestination(const std::string& destination);
	void setDiverted(const int& diverted);
	void setCarrierDelay(const double& carrierDelay);
	void setWeatherDelay(const double& weatherDelay);
	void setNasDelay(const double& nasDelay);
	void setSecurityDelay(const double securityDelay);
	void setAirTime(const double airTime);
	void setArrDelay(const double arrDelay);
	void setOrigin(const std::string origin);
	void setActualElapsedTime(const double actualElapsedTime);

	int getYear() const;
	int getMonth() const;
	int getDayOfMonth() const;
	std::string getDestination() const;
	int getDiverted() const;
	double getCarrierDelay() const;
	double getWeatherDelay() const;
	double getNasDelay() const;
	double getSecurityDelay() const;
	double getAirTime() const;
	double getArrDelay() const;
	std::string getOrigin() const;
	double getActualElapsedTime() const;

	friend std::ostream& operator<<(std::ostream& os, const DelayedFlight& object);

private:
	int m_year{};
	int m_month{};
	int m_dayOfMonth{};
	std::string m_destination{};
	int m_diverted{};
	double m_carrierDelay{};
	double m_weatherDelay{};
	double m_nasDelay{};
	double m_securityDelay{};
	double m_airTime{};
	double m_arrDelay{};
	std::string m_origin{};
	double m_actualElapsedTime{};
};