#include <Util/Listener.h>
#include <Util/CsvParser.h>

#include <fstream>
#include <thread>

int main()
{
  const auto address = boost::asio::ip::make_address("192.168.1.11");
  const unsigned short port = 2751;
  const auto threadsCount = 10;

  // Parse csv file
  const std::string filename = "C:/Users/david/Desktop/CC/cloudcomputing/Server/assets/DelayedFlights.csv";
  CsvParser csvParser(filename);
  try
  {
    std::fstream inputCsv;
    inputCsv.open(filename, std::ios::in);
    std::ofstream outputXml("DelayedFlights.xml");


    outputXml << csvParser.getXmlFromCsv(inputCsv, "DelayedFlight") << '\n';
    std::cout << "Done parsing csv.\n";
    inputCsv.clear();
    inputCsv.close();
    outputXml.close();

    csvParser.setXmlFile("C:/Users/david/Desktop/CC/cloudcomputing/build/Server/DelayedFlights.xml");

    csvParser.parseXml();
    std::cout << "Done parsing xml.\n";
  }
  catch (const std::exception& exception)
  {
    exception.what();
  }


  // The io_context is required for all I/O
  boost::asio::io_context ioContext{ threadsCount };

  // Create and launch a listening port
  std::make_shared<Listener>(ioContext, boost::asio::ip::tcp::endpoint{ address, port })->run();

  // Run the I/O service on the requested number of threads
  std::vector<std::thread> threads;

  threads.reserve(threadsCount - 1);
  for (auto index = 0; index < threadsCount - 1; ++index)
  {
    threads.emplace_back([&ioContext] { ioContext.run(); });
  }

  ioContext.run();

  return EXIT_SUCCESS;
}