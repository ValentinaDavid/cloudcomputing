#include <Response/MostVisitedResponse.h>
#include <Util/DelayedFlight.h>
#include <Util/CsvParser.h>

#include <boost/foreach.hpp>
#include <boost/property_tree/xml_parser.hpp>
#include <boost/property_tree/ptree.hpp>

#include <algorithm>
#include <utility>
#include <string>

class Destination
{
public:
  std::string name;
  int count = 0;

  Destination() 
  {

  }

  Destination(std::string name)
  {
    this->name = name;
  }
};

MostVisitedResponse::MostVisitedResponse() : Response("MostVisitedFlights")
{

}

bool compareDestination(Destination first, Destination second) { return first.count > second.count; }

std::vector<Destination> getDestinations(const std::vector<DelayedFlight>& flights)
{
  std::vector<Destination> destinations;
  for (auto current : flights)
  {
    bool isPresent = false;
    for (auto destCounter : destinations)
    {
      if (destCounter.name == current.getDestination())
      {
        destCounter.count++;
        isPresent = true;
        break;
      }
    }
    if (!isPresent)
      destinations.push_back(Destination(current.getDestination()));
  }
  return destinations;
}

std::string MostVisitedResponse::interpretPacket(const boost::property_tree::ptree& packet)
{
  const std::string filename = "C:/Users/david/Desktop/CC/cloudcomputing/Server/assets/DelayedFlights.csv";
  CsvParser csvParser(filename);
  csvParser.setXmlFile("C:/Users/david/Desktop/CC/cloudcomputing/build/Server/DelayedFlights.xml");

  csvParser.parseXml();
  std::cout << "Done parsing xml.\n";

  std::vector<DelayedFlight> flights = csvParser.getFlights();

  std::vector<Destination> destinations = getDestinations(flights);
  
  std::sort(destinations.begin(), destinations.end(), compareDestination);

  std::string result = "";

  for (int counter = 0; counter < 5; ++counter) {
    result += destinations.at(counter).name + " " + std::to_string(destinations.at(counter).count) + "\n";
  }

  this->content.push_back(boost::property_tree::ptree::value_type("File", "MostVisited.txt"));
  this->content.push_back(boost::property_tree::ptree::value_type("Result", result));
  
  return this->getContentAsString();
}
