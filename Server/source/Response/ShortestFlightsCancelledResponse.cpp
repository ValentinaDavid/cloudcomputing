#include <Response/ShortestFlightsCancelledResponse.h>
#include <Util/DelayedFlight.h>
#include <Util/CsvParser.h>

#include <boost/foreach.hpp>
#include <boost/property_tree/xml_parser.hpp>
#include <boost/property_tree/ptree.hpp>

ShortestFlightsCancelledResponse::ShortestFlightsCancelledResponse() : Response("ShortestFlightsCancelled")
{

}

std::vector<DelayedFlight> getDelayedFlights(const std::vector<DelayedFlight>& flights)
{
	std::vector<DelayedFlight> securityDelayedFlights;
	for (auto& counter : flights)
	{
		if (counter.getSecurityDelay() > 0)
		{
			securityDelayedFlights.push_back(counter);
		}
	}

	return securityDelayedFlights;
}

bool compareFlights(DelayedFlight first, DelayedFlight second)
{
	return first.getAirTime() < second.getAirTime();
}

std::string ShortestFlightsCancelledResponse::interpretPacket(const boost::property_tree::ptree& packet)
{
	const std::string filename = "C:/Users/david/Desktop/CC/cloudcomputing/Server/assets/DelayedFlights.csv";
	CsvParser csvParser(filename);
	csvParser.setXmlFile("C:/Users/david/Desktop/CC/cloudcomputing/build/Server/DelayedFlights.xml");

	csvParser.parseXml();
	std::cout << "Done parsing xml.\n";

	std::vector<DelayedFlight> flights = csvParser.getFlights();

	flights = getDelayedFlights(flights);

	std::sort(flights.begin(), flights.end(), compareFlights);
	std::string result = "[";

	if (flights.size() >= 3)
	{
		for (int counter = 0; counter < 3; ++counter)
		{
			DelayedFlight current = flights.at(counter);
			result += "<" + std::to_string(current.getYear()) + "," + std::to_string(current.getMonth()) + ","
				+ std::to_string(current.getDayOfMonth()) + ","
				+ current.getOrigin() + "," + current.getDestination() + "> ,";
		}
	}
	else if (flights.size() == 0)
	{
		result += "There are no delayed flights due to security!";
	}
	else {
		for (auto counter : flights)
		{
			DelayedFlight current = counter;
			result += "<" + std::to_string(current.getYear()) + "," + std::to_string(current.getMonth()) + ","
				+ std::to_string(current.getDayOfMonth()) + ","
				+ current.getOrigin() + "," + current.getDestination() + "> ," ;
		}
	}

	result += "]";

	this->content.push_back(boost::property_tree::ptree::value_type("File", "ShortestFlightsCancelled.txt"));
	this->content.push_back(boost::property_tree::ptree::value_type("Result", result));

	return this->getContentAsString();
}
