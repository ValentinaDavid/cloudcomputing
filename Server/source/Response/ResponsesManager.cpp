#include <Response/ResponsesManager.h>
#include <Response/HandshakeResponse.h>
#include <Response/MostVisitedResponse.h>
#include <Response/ShortestFlightsCancelledResponse.h>
#include <Response/TotalAirResponse.h>
#include <Response/ShortestFlightsCancelledResponse.h>
#include <Response/MaximumDiversionResponse.h>
#include <Response/AverageResponse.h>
#include <Response/MostCancellationsResponse.h>


ResponsesManager::ResponsesManager()
{
  //this->Responses.emplace("Handshake", std::make_shared<HandshakeResponse>());
  //this->Responses.emplace("Average", std::make_shared<AverageResponse>());
  this->Responses.emplace("MaximumDiversion", std::make_shared<MaximumDiversionResponse>());
  //this->Responses.emplace("MostVisitedFlights", std::make_shared<MostVisitedResponse>());
  //this->Responses.emplace("TotalAir", std::make_shared<TotalAirResponse>());
  //this->Responses.emplace("ShortestFlightsCancelled", std::make_shared<ShortestFlightsCancelledResponse>());
  //this->Responses.emplace("MostCancellations", std::make_shared<MostCancellationsResponse>());
}

std::map<std::string, std::shared_ptr<Framework::Response>> ResponsesManager::getMap() const
{
  return this->Responses;
}
