#include <Response/MaximumDiversionResponse.h>
#include <Util/DelayedFlight.h>
#include <Util/CsvParser.h>

#include <boost/foreach.hpp>
#include <boost/property_tree/xml_parser.hpp>
#include <boost/property_tree/ptree.hpp>

class Route
{
public:
  std::string origin;
  std::string dest;
  double divertedValue;

  Route()
  {

  }

  Route(std::string origin, std::string dest, double divertedValue)
  {
    this->origin = origin;
    this->dest = dest;
    this->divertedValue = divertedValue;
  }
};

MaximumDiversionResponse::MaximumDiversionResponse() : Response("MaximumDiversion")
{

}

bool compareRoutes(Route first, Route second) { return first.divertedValue > second.divertedValue; }

std::vector<Route> getDivertedRoutes(const std::vector<DelayedFlight>& flights)
{
  std::vector<Route> routes;

  for (auto counter : flights)
  {
    if (counter.getDiverted() > 0)
    {
      Route route = Route(counter.getOrigin(), counter.getDestination(), counter.getDiverted());
      routes.push_back(route);
    }
  }

  return routes;
}

std::string MaximumDiversionResponse::interpretPacket(const boost::property_tree::ptree& packet)
{
  const std::string filename = "C:/Users/david/Desktop/CC/cloudcomputing/Server/assets/DelayedFlights.csv";
  CsvParser csvParser(filename);
  csvParser.setXmlFile("C:/Users/david/Desktop/CC/cloudcomputing/build/Server/DelayedFlights.xml");

  csvParser.parseXml();
  std::cout << "Done parsing xml.\n";

  std::vector<DelayedFlight> flights = csvParser.getFlights();
  std::vector<Route> routes = getDivertedRoutes(flights);

  std::sort(routes.begin(), routes.end(), compareRoutes);

  std::string result = "";

  if (routes.size() == 0)
  {
    result += "No deverted routes!";
  }
  else
  {
    result += routes.at(0).origin + "->" + routes.at(0).dest + " with value " + std::to_string(routes.at(0).divertedValue);
  }

  this->content.push_back(boost::property_tree::ptree::value_type("File", "MaximumDiversion.txt"));
  this->content.push_back(boost::property_tree::ptree::value_type("Result", result));

  return this->getContentAsString();
}