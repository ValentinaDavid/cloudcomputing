#include <Response/AverageResponse.h>
#include <Util/DelayedFlight.h>
#include <Util/CsvParser.h>

#include <boost/foreach.hpp>
#include <boost/property_tree/xml_parser.hpp>
#include <boost/property_tree/ptree.hpp>

#include <algorithm>
#include <utility>
#include <string>

AverageResponse::AverageResponse() : Response("AverageResponse")
{

}

double getAverageOfDelays(std::vector<DelayedFlight>& flights) 
{
  double NASDelay = 0;
  double carrierDelay = 0;

  for (auto& flight : flights) 
  {
    if (flight.getDayOfMonth() >= 15)
    {
      NASDelay += flight.getNasDelay();
      carrierDelay += flight.getCarrierDelay();
    }
  }

  return (NASDelay + carrierDelay) / 2;
}

std::string AverageResponse::interpretPacket(const boost::property_tree::ptree& packet)
{
  const std::string filename = "C:/Users/david/Desktop/CC/cloudcomputing/Server/assets/DelayedFlights.csv";
  CsvParser csvParser(filename);
  csvParser.setXmlFile("C:/Users/david/Desktop/CC/cloudcomputing/build/Server/DelayedFlights.xml");

  csvParser.parseXml();
  std::cout << "Done parsing xml.\n";

  std::vector<DelayedFlight> flights = csvParser.getFlights();

  std::string result = "";

  result += std::to_string(getAverageOfDelays(flights));

  this->content.push_back(boost::property_tree::ptree::value_type("File", "Average.txt"));
  this->content.push_back(boost::property_tree::ptree::value_type("Result", result));

  return this->getContentAsString();
}