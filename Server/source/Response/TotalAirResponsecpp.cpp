#include <Response/TotalAirResponse.h>
#include <Util/CsvParser.h>
#include <Util/DelayedFlight.h>

#include <boost/foreach.hpp>
#include <boost/property_tree/xml_parser.hpp>
#include <boost/property_tree/ptree.hpp>


class Destination 
{
public:
  std::string name;
  double totalArrDelay = 0;
  double actualElapsedTime = 0;

  Destination() 
  {

  }

  Destination(std::string name)
  {
    this->name = name;
  }
};

TotalAirResponse::TotalAirResponse() : Response("TotalAir")
{

}

std::string TotalAirResponse::interpretPacket(const boost::property_tree::ptree& packet)
{
  const std::string filename = "C:/Users/david/Desktop/CC/cloudcomputing/Server/assets/DelayedFlights.csv";
  CsvParser csvParser(filename);
  csvParser.setXmlFile("C:/Users/david/Desktop/CC/cloudcomputing/build/Server/DelayedFlights.xml");

  csvParser.parseXml();
  std::cout << "Done parsing xml.\n";

  std::vector<DelayedFlight> flights = csvParser.getFlights();
  Destination dest = Destination("BWI");
  
  for (auto counter : flights) 
  {
    if (counter.getDestination() == dest.name)
    {
      std::cout << "98/n";
      dest.totalArrDelay += counter.getArrDelay();
      dest.actualElapsedTime += counter.getActualElapsedTime();
    }
  }

  std::string result = "";

  result += "The total elapsed time is: " + std::to_string(dest.actualElapsedTime) + " and the total arr delay is: " + std::to_string(dest.totalArrDelay) + "\n";

  this->content.push_back(boost::property_tree::ptree::value_type("File", "TotalAirAndElapsedTime.txt"));
  this->content.push_back(boost::property_tree::ptree::value_type("Result", result));

  return this->getContentAsString();
}