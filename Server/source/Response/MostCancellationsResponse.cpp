#include <Response/MostCancellationsResponse.h>
#include <Util/DelayedFlight.h>
#include <Util/CsvParser.h>

#include <boost/foreach.hpp>
#include <boost/property_tree/xml_parser.hpp>
#include <boost/property_tree/ptree.hpp>

#include <algorithm>
#include <utility>
#include <string>

MostCancellationsResponse::MostCancellationsResponse() : Response("MostCancellationsResponse")
{

}

class Month 
{
public:
  int name;
  int count = 0;

  Month() 
  {

  }

  Month(const int& name)
  {
    this->name = name;
  }
};

std::vector<Month> getMonths(std::vector<DelayedFlight>& flights)
{
  std::vector<Month> months;
  for (auto flight : flights)
  {
    if (flight.getWeatherDelay() > 0) 
    {
      bool isPresent = false;
      for (auto& month : months) 
      {
        if (flight.getMonth() == month.name) 
        {
          month.count++;
          std::cout << month.name << " " << month.count << "\n";
          isPresent = true;
          break;
        }
      }
      if (!isPresent)
      {
        std::cout << "new month added\n";
        months.push_back(Month(flight.getMonth()));
      }
    }
  }
  return months;
}

bool compareMonths(Month& first, Month& second) { return first.count > second.count; }

std::string MostCancellationsResponse::interpretPacket(const boost::property_tree::ptree& packet)
{
  const std::string filename = "C:/Users/david/Desktop/CC/cloudcomputing/Server/assets/DelayedFlights.csv";
  CsvParser csvParser(filename);
  csvParser.setXmlFile("C:/Users/david/Desktop/CC/cloudcomputing/build/Server/DelayedFlights.xml");

  csvParser.parseXml();
  std::cout << "Done parsing xml.\n";

  std::vector<DelayedFlight> flights = csvParser.getFlights();

  std::vector<Month> months = getMonths(flights);
  std::string result = "";


  if (months.size() > 0) {
    std::sort(months.begin(), months.end(), compareMonths);
    result += "Month: " + std::to_string(months.at(0).name) + ", number of cancellations: " + std::to_string(months.at(0).count);
  }
  else
    result += "No cancellations found\n";

  this->content.push_back(boost::property_tree::ptree::value_type("File", "MostCancellations.txt"));
  this->content.push_back(boost::property_tree::ptree::value_type("Result", result));

  return this->getContentAsString();
}
