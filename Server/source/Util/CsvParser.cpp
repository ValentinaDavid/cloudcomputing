#include <Util/CsvParser.h>

#include <boost/algorithm/string.hpp>
#include <boost/tokenizer.hpp>
#include <boost/foreach.hpp>
#include <boost/property_tree/xml_parser.hpp>
#include <boost/property_tree/ptree.hpp>

#include <sstream>
#include <fstream>

#define ATTR_SET ".<xmlattr>"

CsvParser::CsvParser(const std::string& filename)
{
  m_csvFile = filename;
}

std::vector<std::string> CsvParser::splitFields(const std::string& csvFile)
{
  boost::tokenizer< boost::escaped_list_separator<char> > tokenizer(csvFile);
  std::vector<std::string> lines{ tokenizer.begin(), tokenizer.end() };

  for (std::string& tok : lines)
  {
    boost::trim(tok);
  }

  return lines;
}

std::string CsvParser::getXmlFromCsv(const std::string& csvFile, const std::vector<std::string>& fieldNames, const std::string& itemTag ,
  const std::string& defFieldTag)
{
  std::string result = '<' + itemTag + ">\n";

  std::vector<std::string> fields = splitFields(csvFile);

  for (std::size_t index = 1; index < fields.size(); ++index)
  {
    if (!fields[index].empty())
    {
      const std::string& tag = index < fieldNames.size() ? fieldNames[index] : defFieldTag;

      result += "    <" + tag + '>' + fields[index] + "</" + tag + ">\n";
    }
  }

  return result + "</" + itemTag + ">\n";
}

std::string CsvParser::getXmlFromCsv(std::istream& csvStream, const std::string& itemTag,
  bool hasHeader, const std::string& defFieldTag)
{
  std::string xml;
  xml += "<Flights>\n";

  std::string line;

  if (hasHeader) while (std::getline(csvStream, line) && line.empty());

  const auto fieldNames = splitFields(line);

  // skip empty column index
  int index = 1;

  while (std::getline(csvStream, line) && index < 50)
  {
    if (!line.empty())
    {
      xml += getXmlFromCsv(line, fieldNames, itemTag, defFieldTag);
    }
    index++;
  }

  xml += "</Flights>";

  return xml;
}

void CsvParser::parseXml()
{
  using boost::property_tree::ptree;
  std::fstream inputXml;
  inputXml.open(m_xmlFile, std::ios::in);
  read_xml(inputXml, m_tree);

  BOOST_FOREACH(ptree::value_type& const value, m_tree.get_child("Flights"))
  {
    if (value.first == "DelayedFlight") {
      int year = value.second.get<int>("Year");
      int month = value.second.get<int>("Month");
      int dayOfMonth = value.second.get<int>("DayofMonth");
      std::string destination = value.second.get<std::string>("Dest");
      int diverted = value.second.get<int>("Diverted");
      double carrierDelay = value.second.get<double>("CarrierDelay");
      double weatherDelay = value.second.get<double>("WeatherDelay");
      double nasDelay = value.second.get<double>("NASDelay");
      double securityDelay = value.second.get<double>("SecurityDelay");
      double arrDelay = value.second.get<double>("ArrDelay");
      std::string origin = value.second.get<std::string>("Origin");
      double actualElapsedTime = value.second.get<double>("ActualElapsedTime");

      DelayedFlight currentFlight;
      currentFlight.setYear(year);
      currentFlight.setMonth(month);
      currentFlight.setDayOfMonth(dayOfMonth);
      currentFlight.setDestination(destination);
      currentFlight.setDiverted(diverted);
      currentFlight.setCarrierDelay(carrierDelay);
      currentFlight.setWeatherDelay(weatherDelay);
      currentFlight.setNasDelay(nasDelay);
      currentFlight.setArrDelay(arrDelay);
      currentFlight.setOrigin(origin);
      currentFlight.setActualElapsedTime(actualElapsedTime);

      m_flights.push_back(currentFlight);
    }
  }
}

void CsvParser::setXmlFile(const std::string& xmlFile)
{
  m_xmlFile = xmlFile;
}

std::vector<DelayedFlight> CsvParser::getFlights() {
  return this->m_flights;
}