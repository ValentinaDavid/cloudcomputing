#include <Util/DelayedFlight.h>

#include <string>

void DelayedFlight::setYear(const int& year)
{
	m_year = year;
}

void DelayedFlight::setMonth(const int& month)
{
	m_month = month;
}

void DelayedFlight::setDayOfMonth(const int& dayOfMonth)
{
	m_dayOfMonth = dayOfMonth;
}

void DelayedFlight::setDestination(const std::string& destination)
{
	m_destination = destination;
}

void DelayedFlight::setDiverted(const int& diverted)
{
	m_diverted = diverted;
}

void DelayedFlight::setCarrierDelay(const double& carrierDelay)
{
	m_carrierDelay = carrierDelay;
}

void DelayedFlight::setWeatherDelay(const double& weatherDelay)
{
	m_weatherDelay = weatherDelay;
}

void DelayedFlight::setNasDelay(const double& nasDelay)
{
	m_nasDelay = nasDelay;
}

void DelayedFlight::setSecurityDelay(const double securityDelay)
{
	m_securityDelay = securityDelay;
}

void DelayedFlight::setAirTime(const double airTime)
{
	m_airTime = airTime;
}

void DelayedFlight::setArrDelay(const double arrDelay)
{
	m_arrDelay = arrDelay;
}

void DelayedFlight::setOrigin(const std::string origin)
{
	m_origin = origin;
}

void DelayedFlight::setActualElapsedTime(const double actualElapsedTime)
{
	m_actualElapsedTime = actualElapsedTime;
}

int DelayedFlight::getYear() const
{
	return m_year;
}

int DelayedFlight::getMonth() const
{
	return m_month;
}

int DelayedFlight::getDayOfMonth() const
{
	return m_dayOfMonth;
}

std::string DelayedFlight::getDestination() const
{
	return m_destination;
}

int DelayedFlight::getDiverted() const
{
	return m_diverted;
}

double DelayedFlight::getCarrierDelay() const
{
	return m_carrierDelay;
}

double DelayedFlight::getWeatherDelay() const
{
	return m_weatherDelay;
}

double DelayedFlight::getNasDelay() const
{
	return m_nasDelay;
}

double DelayedFlight::getSecurityDelay() const
{
	return m_securityDelay;
}

double DelayedFlight::getAirTime() const
{
	return m_airTime;
}

double DelayedFlight::getArrDelay() const
{
	return m_arrDelay;
}

std::string DelayedFlight::getOrigin() const
{
	return m_origin;
}

double DelayedFlight::getActualElapsedTime() const
{
	return m_actualElapsedTime;
}

std::ostream& operator<<(std::ostream& os, const DelayedFlight& object)
{
	os << "[" << object.m_year << "," << object.m_month << "," << object.m_dayOfMonth << "," << object.m_destination << "," << object.m_diverted << "," << object.m_arrDelay << "," << object.m_carrierDelay << "," << object.m_nasDelay << "," << object.m_securityDelay << "," << object.m_weatherDelay << "," << object.getOrigin() << "," << object.m_actualElapsedTime << "]";
	return os;
}