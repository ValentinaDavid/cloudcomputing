#include <Request/RequestsManager.h>
#include <Request/HandshakeRequest.h>
#include <Request/MostVisitedRequest.h>
#include <Request/TotalAirRequest.h>
#include <Request/ShortestFlightsCancelledRequest.h>
#include <Request/MaximumDiversionRequest.h>
#include <Request/AverageRequest.h>
#include <Request/MostCancellationsRequest.h>

RequestsManager::RequestsManager()
{
  //this->requests.emplace("Handshake", std::make_shared<HandshakeRequest>());
  //this->requests.emplace("Average", std::make_shared<AverageRequest>());
  //this->requests.emplace("MostVisitedFlights", std::make_shared<MostVisitedRequest>());
  //this->requests.emplace("TotalAir", std::make_shared<TotalAirRequest>());
  //this->requests.emplace("ShortestFlightsCancelled", std::make_shared<ShortestFlightsCancelledRequest>());
  this->requests.emplace("MaximumDiversion", std::make_shared<MaximumDiversionRequest>());
  //this->requests.emplace("MostCancellations", std::make_shared<MostCancellationsRequest>());
}

std::map<std::string, std::shared_ptr<Framework::Request>> RequestsManager::getMap() const
{
  return this->requests;
}
