#include <Util/Session.h>

#include <iostream>
#include <memory>

int main(int argc, char** argv)
{
  const auto host = "192.168.1.11";
  const auto port = "2751";

  // The io_context is required for all I/O
  boost::asio::io_context ioContext;

  RequestsManager requestsManager;

  //auto handshake = requestsManager.getMap().at("Handshake")->getContentAsString();
  //auto average = requestsManager.getMap().at("Average")->getContentAsString();
  auto maximumDiversion = requestsManager.getMap().at("MaximumDiversion")->getContentAsString();
  //auto mostCancellations = requestsManager.getMap().at("MostCancellations")->getContentAsString();
  //auto mostVisited = requestsManager.getMap().at("MostVisitedFlights")->getContentAsString();
  //auto shortestFlightsCancelled = requestsManager.getMap().at("ShortestFlightsCancelled")->getContentAsString();
  //auto totalAir = requestsManager.getMap().at("TotalAir")->getContentAsString();

  // Launch the asynchronous operation
  //std::make_shared<Session>(ioContext)->run(host, port, handshake);
 // std::make_shared<Session>(ioContext)->run(host, port, average);
  std::make_shared<Session>(ioContext)->run(host, port, maximumDiversion);
  //std::make_shared<Session>(ioContext)->run(host, port, mostCancellations);
  //std::make_shared<Session>(ioContext)->run(host, port, mostVisited);
  //std::make_shared<Session>(ioContext)->run(host, port, shortestFlightsCancelled);
  //std::make_shared<Session>(ioContext)->run(host, port, totalAir);

  // Run the I/O service. The call will return when
  // the socket is closed.
  ioContext.run();

  return EXIT_SUCCESS;
}